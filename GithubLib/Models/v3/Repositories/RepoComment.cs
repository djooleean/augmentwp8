﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GithubLib.Models.v3.Repositories
{
    public class RepoComment
    {
        public string url { get; set; }
        public string html_url { get; set; }
        public int id { get; set; }
        public User user { get; set; }
        public int? position { get; set; }
        public int? line { get; set; }
        public string path { get; set; }
        public string commit_id { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public string body { get; set; }
    }
}
