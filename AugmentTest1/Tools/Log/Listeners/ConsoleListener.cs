﻿
namespace AugmentTest1.Tools.Log.Listeners
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Threading.Tasks;

    public class ConsoleListener : ILogListener
    {
        #region Private members

        readonly string _errorFormat = "Error [{0}] >> {1}, {2} : L.{3}";
        readonly string _msgFormat = "{0}: {1} ({2} - L.{3})";

        #endregion

        #region Contructor

        /// <summary>
        /// Initialize a new console listener. By default the log FILTER is set to 'Information'.
        /// </summary>
        public ConsoleListener()
        {
            Filter = LogLevels.Information;
        }

        #endregion

        /// <summary>
        /// Gets / sets the current listener's filter
        /// </summary>
        public LogLevels Filter
        {
            get; set;
        }


        /// <summary>
        /// Log an exception with the default debugger console
        /// </summary>
        /// <param name="exception"></param>
        /// <param name="memberName"></param>
        /// <param name="sourceFilePath"></param>
        /// <param name="sourceLineNumber"></param>
        /// <returns></returns>
        public Task ErrorAsync(Exception exception, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
        {
            var errorDescription = string.Format(_errorFormat, exception.GetType().Name, memberName, sourceFilePath, sourceLineNumber);

            Debug.WriteLine(errorDescription);

            Debug.WriteLine(exception.Message);

            return Task.FromResult(true);
        }

        public Task LogAsync(LogLevels level, string message, Dictionary<string, string> tags, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
        {
            if (level < Filter)
                return Task.FromResult(true);

            var msg = string.Format(_msgFormat, level.ToString(), message, sourceFilePath, sourceLineNumber);

            Debug.WriteLine(msg);

            return Task.FromResult(true);
        }

    }


}
