﻿
namespace AugmentTest1.Tools
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.Runtime.InteropServices.WindowsRuntime;
    using System.Threading;
    using System.Threading.Tasks;
    using Windows.Foundation;
    using Windows.UI.Xaml.Data;

    public abstract class IncrementalLoadingDataSource : ObservableCollection<object>, ISupportIncrementalLoading, INotifyCollectionChanged
    {

        #region ISupportIncrementalLoading

        public bool HasMoreItems
        {
            get { return HasMoreItemsOverride(); }
        }

        public Windows.Foundation.IAsyncOperation<LoadMoreItemsResult> LoadMoreItemsAsync(uint count)
        {
            if (_busy)
                return default(Windows.Foundation.IAsyncOperation<LoadMoreItemsResult>);

            _busy = true;

            return AsyncInfo.Run((c) => LoadMoreItemsAsync(c, count));
        }

        #endregion
        

        #region Private methods

        async Task<LoadMoreItemsResult> LoadMoreItemsAsync(CancellationToken c, uint count)
        {
            try
            {
                var items = await LoadMoreItemsOverrideAsync(c, count);
                var baseIndex = this.Count;

                foreach (var item in items)
                    this.Add(item);
                //_storage.AddRange(items);

                // Now notify of the new items
                //NotifyOfInsertedItems(baseIndex, items.Count);

                return new LoadMoreItemsResult { Count = (uint)items.Count };
            }
            finally
            {
                _busy = false;
            }
        }

        //void NotifyOfInsertedItems(int baseIndex, int count)
        //{
        //    if (CollectionChanged == null)
        //    {
        //        return;
        //    }

        //    for (int i = 0; i < count; i++)
        //    {
        //        var args = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, _storage[i + baseIndex], i + baseIndex);
        //        CollectionChanged(this, args);
        //    }
        //}

        #endregion

        #region Overridable methods

        protected abstract Task<IList<object>> LoadMoreItemsOverrideAsync(CancellationToken c, uint count);

        protected abstract bool HasMoreItemsOverride();

        #endregion

        #region State

        //List<object> _storage = new List<object>();

        bool _busy = false;

        #endregion

    }

}
