﻿
namespace AugmentTest1.ViewModels
{
    using GithubLib.Models.v3.Repositories;
    using System.Collections.ObjectModel;
    using Tools;
    public interface IRepoListViewModel
    {

        PublicRepository SelectedRepository { get; set;}

        bool SelectedItemEnabled { get; }

        //ObservableCollection<PublicRepository> Repositories { get; }
        RepoList Repositories { get; }

        DelegateCommand ViewRepoDetailsCommand { get; }

    }

}
