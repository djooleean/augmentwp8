﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GithubLib.Models.v3.RateLimit
{

    public class CurrentRate
    {
        public Resources resources { get; set; }

        public Rate rate { get; set; }
    }
}
