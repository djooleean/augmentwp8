﻿
using System.Runtime.Serialization;

namespace AugmentTest1.Core.AppContext
{

    [DataContract]
    public class AugmentAppStateData : IAppStateData
    {

        public AugmentAppStateData()
        {
            Reset();
        }

        public void Reset()
        {
            IsLogged = false;
            UserLogin = string.Empty;
        }

        [DataMember]
        public bool IsLogged
        {
            get; set;
        }

        [DataMember]
        public string UserLogin
        {
            get; set;
        }



    }


}
