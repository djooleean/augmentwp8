﻿
namespace AugmentTest1.Tools
{

    using Windows.ApplicationModel.Resources;

    public static class LocalizedStrings
    {
        public static string Get(string key)
        {
            if(!string.IsNullOrEmpty(key))
                return ResourceLoader.GetForCurrentView().GetString(key);
            return string.Empty;
        }
    }
}
