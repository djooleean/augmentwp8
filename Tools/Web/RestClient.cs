﻿
namespace Tools.Web
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using Web;

    /// <summary>
    /// REST client that manage communication with REST/JSON web services
    /// </summary>
    public class RestClient : IRestClient, IDisposable
    {
        #region Properties

        /// <summary>
        /// Gets the http client
        /// </summary>
        public HttpClient Client
        {
            protected set;
            get;
        }

        #endregion

        #region Constructor / init methods

        /// <summary>
        /// REST client constructor that needs the base URI for the web service to call and a timeout for the asynchronous operations.
        /// </summary>
        /// <param name="wsBaseUri">The base URI address of the REST web service to call</param>
        /// <param name="timeoutMs">The timeout delay in milliseconds</param>
        /// <exception cref="System.ArgumentException">If 'wsBaseUri' is invalid</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">If 'timeoutMs' is invalid</exception>
        public RestClient(string wsBaseUri, int timeoutMs = HttpConstants.DefaultRestClientTimeoutMs)
        {
            if (string.IsNullOrEmpty(wsBaseUri))
                throw new ArgumentException("RestClient.Ctor() : wsBaseUri");
            if (timeoutMs < 0)
                throw new ArgumentOutOfRangeException("RestClient.Ctor() : timeoutMs");
                        
            // Perform client's inner initialization
            InitializeClient(wsBaseUri, timeoutMs);
        }

        /// <summary>
        /// Initializes inner HTTP client
        /// </summary>
        /// <param name="serviceBaseAddress">The base URI address of the REST web service to call</param>
        /// <param name="timeoutMs">The timeout delay in milliseconds</param>
        private void InitializeClient(string serviceBaseAddress, int timeoutMs)
        {
            // generate the underlying  HTTP client with target server @
            Client = new HttpClient
            {
                BaseAddress = new Uri(serviceBaseAddress),
                Timeout = TimeSpan.FromMilliseconds(timeoutMs)
            };

            // Init REQUESTS headers
            //Client.DefaultRequestHeaders.Clear();
            //Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.HttpRequest.Header_ContentType));
            //Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.HttpRequest.Header_Accept));
            //Client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:19.0) Gecko/20100101 Firefox/19.0");
        }

        #endregion

        #region IRestClient implementation

        /// <summary>
        /// Send a GET request to the desired REST web service (defined by the 'endpoint' parameter).
        /// </summary>
        /// <typeparam name="T">Generic type of the expected response</typeparam>
        /// <param name="endpoint"></param>
        /// <param name="cancelToken"></param>        
        /// <returns></returns>
        public async Task<GenericRestResponse<T>> HttpGetAsync<T>(
            string endpoint, 
            Dictionary<string, string> parameters,
            Dictionary<string, string> requestHeaders,
            CancellationToken cancelToken = new CancellationToken()
            )
        {
            GenericRestResponse<T> result = new GenericRestResponse<T>();
            Exception exception = null;

            try
            {
                // init GET request headers
                InitializeHttpHeaders(requestHeaders);

                // Perform [GET] request
                //
                var endPointRelativeUri = ComputeRelativeWsUri(endpoint, parameters);
                var response = await Client.GetAsync(endPointRelativeUri, cancelToken);

                // Read response request headers
                result.IsSuccess        = response.IsSuccessStatusCode;
                result.ResponseMessage  = response.ReasonPhrase;
                foreach (var header in response.Headers)
                    result.ResponseHeaders.Add(header.Key, string.Join(HttpConstants.Headers.ResponseHeaders_Separator, header.Value));

                // CHECK RESPONSE DATA
                if (response.IsSuccessStatusCode)
                {
                    // read & deserialize JSON content
                    var jsonStream = await response.Content.ReadAsStreamAsync();
                    using (StreamReader reader = new StreamReader(jsonStream, Encoding.UTF8))
                    {
                        var json = await reader.ReadToEndAsync();
                        var data = JsonConvert.DeserializeObject<T>(json);
                        result.ResponseData = data;
                    }
                }
                else
                {
                    var errorMsg = string.Format("StatusCode: {0}, ReasonPhrase: {1}", response.StatusCode, response.ReasonPhrase);

                    // generate our exception instance
                    exception = (response.StatusCode == System.Net.HttpStatusCode.RequestTimeout)
                        ? new TimeoutException()
                        : new Exception(errorMsg);
                }
            }
            catch (Exception ex)
            {
                // unexpected exception
                exception = ex;
            }

            // If there is an exception => throw it
            if (exception != null)
            {
                throw exception;
            }

            // else return generic result
            return result;
        }

        /// <summary>
        /// Send a POST request to the desired REST web service (defined by the 'endpoint' parameter).
        /// </summary>
        /// <typeparam name="T">Generic type of the response returned by the POST command</typeparam>
        /// <param name="endpoint">URL to target</param>
        /// <param name="parameters">URL parameters used with the POST operation</param>
        /// <param name="data">The body data</param>
        /// <param name="cancelToken">Cancellation token to interrupt the asynchronous POST</param>
        /// <returns>Generic Response of type T</returns>
        public async Task<GenericRestResponse<T>> HttpPostAsync<T>(
            string endpoint, 
            Dictionary<string, string> parameters,
            Dictionary<string, string> requestHeaders,
            object data, 
            CancellationToken cancelToken)
        {
            GenericRestResponse<T> result = new GenericRestResponse<T>();
            Exception exception = null;

            try
            {
                // Init headers
                InitializeHttpHeaders(requestHeaders);

                // Perform & Execute request (& serialize content to JSON)
                var jsonQuery = JsonConvert.SerializeObject(data);
                Debug.WriteLine("HttpPost : {0}, jsonQuery : {1}", endpoint, jsonQuery);

                // Generates the POST content containing the serialized JSON
                var contentQuery = new StringContent(jsonQuery, Encoding.UTF8, HttpConstants.DefaultMediaType);

                // Perform the async post
                var endPointRelativeUri = ComputeRelativeWsUri(endpoint, parameters);
                var response = await Client.PostAsync(endPointRelativeUri, contentQuery, cancelToken);

                // Check response:
                if (response.IsSuccessStatusCode)
                {
                    #region successfull response

                    // deserialize result
                    var bytesResult = await response.Content.ReadAsByteArrayAsync();
                    var jsonResult = Encoding.UTF8.GetString(bytesResult, 0, bytesResult.Length * sizeof(byte));

                    // TODO: log
                    Debug.WriteLine("HttpPost : {0}, SuccessResponse : {1}", endpoint, jsonResult);

                    if (!string.IsNullOrEmpty(jsonResult))
                    {
                        // Gets the deserialized result
                        var dataResult = JsonConvert.DeserializeObject<T>(jsonResult);
                        result.ResponseData = dataResult;
                    }

                    #endregion
                }
                else
                {
                    #region Bad request

                    if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                    {
                        var bytesResult = await response.Content.ReadAsByteArrayAsync();
                        var jsonResult = Encoding.UTF8.GetString(bytesResult, 0, bytesResult.Length * sizeof(byte));

                        Debug.WriteLine("HttpPost : {0}, FailureResponse : {1}", endpoint, jsonResult);

                        if (!string.IsNullOrEmpty(jsonResult))
                        {
                            var dataResult = JsonConvert.DeserializeObject<T>(jsonResult);
                            result.ResponseData = dataResult;
                        }
                    }
                    // Other Errors
                    else
                    {
                        Debug.WriteLine("HttpPost : {0}, ErrorResponse : {1} - {2}", endpoint, response.StatusCode, response.ReasonPhrase);

                        exception = (response.StatusCode == System.Net.HttpStatusCode.RequestTimeout)
                            ? new TimeoutException()
                            : new Exception(string.Format("StatusCode: {0}, ReasonPhrase: {1}", response.StatusCode, response.ReasonPhrase));
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                exception = ex;
            }

            if (exception != null)
            {
                throw exception;
            }

            return result;
        }

        #endregion

        #region IDispoable implementation

        public void Dispose()
        {
            if (Client != null)
            {
                Client.Dispose();
                Client = null;
            }
        }

        #endregion

        /// <summary>
        /// Concatenates the relative REST service endpoint Uri with all request parameters
        /// </summary>
        /// <param name="wsEndpoint">Service endpoint relative URI string</param>
        /// <param name="requestParameters">All the request parameters</param>
        /// <returns>Concatenated relative endpoint uri with parameters</returns>
        private string ComputeRelativeWsUri(string wsEndpoint, Dictionary<string, string> requestParameters)
        {
            string relUri = wsEndpoint;

            if (requestParameters == null || requestParameters.Count == 0)
                return relUri;
            else
            {
                relUri += "?";
                foreach (var param in requestParameters)
                {
                    relUri += param.Key + "=" + param.Value + "&";
                }
                // remove unwanted last '&'
                relUri = relUri.Remove(relUri.Length - 1);
            }

            return relUri;
        }

        private void InitializeHttpHeaders(Dictionary<string, string> headers)
        {
            if (Client == null)
                return;

            // Clean to default
            Client.DefaultRequestHeaders.Clear();
            Client.DefaultRequestHeaders.TryAddWithoutValidation(HttpConstants.Headers.UserAgent, HttpConstants.DefaultUserAgent);

            if (headers == null || headers.Count == 0)
                return;

            // Add custom headers
            foreach(var header in headers)
            {
                Client.DefaultRequestHeaders.TryAddWithoutValidation(header.Key, header.Value);
            }
        }
    }
}
