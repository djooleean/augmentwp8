﻿
namespace AugmentTest1.Views
{
    using AugmentTest1;
    using Core.AppContext;
    using Ninject;
    using System;
    using Tools.Log;
    using ViewModels;
    using Windows.UI.Xaml.Controls;
    using Windows.UI.Xaml.Navigation;

    public abstract class BasePage : Page
    {
        private ILogger _logger;

        public BasePage()
        {
            _logger = App.IOC.Get<ILogger>();
        }

        private async void HardwareButtons_BackPressed(object sender, Windows.Phone.UI.Input.BackPressedEventArgs e)
        {
            try
            {
                var selectedVm = App.IOC.Get<BaseViewModel>(this.GetType().Name);

                var res = await selectedVm.ManageBackKeyPressedAsync();

                e.Handled = res;
            }
            catch(Exception)
            {         
                // in case of exception: follow system behavior       
                e.Handled = false;
            }
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            // handle Bak Key
            Windows.Phone.UI.Input.HardwareButtons.BackPressed += HardwareButtons_BackPressed;

            // Initialize binding between the page and its ViewModel
            //
            Exception error = null;
            try
            {
                var vm = this.DataContext as BaseViewModel;
                if (vm != null)
                {
                    // Auto refresh view model on page shown
                    bool res = await vm.RefreshAsync();
                }
                else
                {
                    // Full VM loading
                    var selectedVm = App.IOC.Get<BaseViewModel>(this.GetType().Name);

                    DataContext = selectedVm;

                    await selectedVm.LoadAsync();
                }
            }
            catch(Exception ex)
            {
                error = ex;                
            }
            finally
            {
                // log our navigation exception
                if(_logger != null && error != null)
                    await _logger.ErrorAsync(error);
            }
        }

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            base.OnNavigatingFrom(e);

            // handle Bak Key
            Windows.Phone.UI.Input.HardwareButtons.BackPressed -= HardwareButtons_BackPressed;

            // Clean current page
            Clean();

            // save app state
            var appState = App.IOC.Get<IAppState>();
            if (appState != null) appState.SaveCurrentState();

            // Clean its view model
            var vm = this.DataContext as BaseViewModel;
            if (vm != null)
            {
                vm.Clean();
            }
        }

        public virtual void Clean()
        {
            
        }


    }
}
