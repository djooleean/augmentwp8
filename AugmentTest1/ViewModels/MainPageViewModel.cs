﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using AugmentTest1.Core.Navigation;
using AugmentTest1.Tools.Log;
using AugmentTest1.Tools;
using GithubLib.API;
using AugmentTest1.Views;
using AugmentTest1.Core.AppContext;


namespace AugmentTest1.ViewModels
{
    public class MainPageViewModel : BaseViewModel, IMainPageViewModel
    {
        #region Private members

        ILogger _logger;        
        IGithubClient _gitClient;
        DelegateCommand _loginCommand;
        DelegateCommand _anonymousLoginCommand;
        private string _login;
        private string _pwd;

        #endregion

        #region Constructor

        /// <summary>
        /// The main page VM managing Github repositories.
        /// </summary>
        /// <param name="navigationService">Needed navigation service</param>
        public MainPageViewModel(
            INavigationService navigationService,
            ILogger logger,
            IGithubClient gitClient,
            IAppState appState
            ) 
            : base(navigationService, appState)
        {
            Expect.NotNull(logger, "Logger");
            Expect.NotNull(gitClient, "gitClient");


            _logger = logger;
            _gitClient = gitClient;

            InitializeCommands();
        }

        private void InitializeCommands()
        {
            LoginCommand = new DelegateCommand(
                (param) => ExecuteLoginCommand(),
                (param) => CanExecuteLoginCommand(param)
                );

            AnonymousLoginCommand = new DelegateCommand(
                (param) => ExecuteAnonymousCommand(),
                (param) => { return true; }
                );
        }

        #endregion

        #region Public properties

        public string Login
        {
            get
            {
                return _login;
            }

            set
            {
                Set(ref _login, value);
                LoginCommand.RaiseCanExecuteChanged();
            }
        }

        public string Password
        {
            get
            {
                return _pwd;
            }

            set
            {
                Set(ref _pwd, value);
                LoginCommand.RaiseCanExecuteChanged();
            }
        }

        public DelegateCommand LoginCommand
        {
            get
            {
                return _loginCommand;
            }
            protected set
            {
                Set(ref _loginCommand, value);
            }
        }

        public DelegateCommand AnonymousLoginCommand
        {
            get
            {
                return _anonymousLoginCommand;
            }
            protected set
            {
                Set(ref _anonymousLoginCommand, value);
            }
        }

        #endregion

        #region Commands definitions

        private bool CanExecuteLoginCommand(object param)
        {
            return (!string.IsNullOrEmpty(Login) && !string.IsNullOrEmpty(Password));
        }

        private async void ExecuteLoginCommand()
        {
            // already busy
            if (IsLoading)
                return;
            
            try
            {
                IsLoading = true;

                // Check GitHub credentials
                var result = 
                    await _gitClient.LoginAsync(Login, Password);

                if(result)
                {
                    // Save app context
                    CurrentState.StateData.IsLogged     = true;
                    CurrentState.StateData.UserLogin    = Login;

                    // navigate to next page
                    NavService.Navigate(typeof(RepoListPage));
                }
                else
                {
                    CurrentState.StateData.IsLogged     = false;
                    CurrentState.StateData.UserLogin    = Login;

                    // error message: not logged
                    await NavService.ShowPopup(LocalizedStrings.Get("msgLoginError"));
                }
            }
            finally
            {
                IsLoading = false;
            }
        }

        private void ExecuteAnonymousCommand()
        {
            try
            {
                IsLoading = true;

                // Save app context
                CurrentState.StateData.IsLogged     = false;
                CurrentState.StateData.UserLogin    = string.Empty;

                // navigate to next page
                NavService.Navigate(typeof(RepoListPage));
            }
            finally
            {
                IsLoading = false;
            }

        }

        #endregion


        public override Task<bool> LoadAsync()
        {
            if (IsLoading == true)
                return Task.FromResult(false);

            try
            {
                IsLoading = true;

                Login = CurrentState.StateData.UserLogin;
                Password = string.Empty;

                return Task.FromResult(true);
            }
            finally
            {
                IsLoading = false;
            }
        }

        public override Task<bool> RefreshAsync()
        {
            // in this case: the same as LoadAsync:
            return LoadAsync();
        }

        public override void Clean()
        {
            
        }

        protected override Task OnBackKeyPressedAsync()
        {
            return Task.FromResult(true);
        }
    }
}
