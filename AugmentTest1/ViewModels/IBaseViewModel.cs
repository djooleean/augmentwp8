﻿
namespace AugmentTest1.ViewModels
{
    using Core.Navigation;
    using System.Threading.Tasks;

    public interface IBaseViewModel
    {
        bool IsLoading { get; }

        string LoadingMessage { get; set; }

        INavigationService NavService { get; }


        Task<bool> ManageBackKeyPressedAsync();

        Task<bool> LoadAsync();

        Task<bool> RefreshAsync();

        void Clean();
    }
}
