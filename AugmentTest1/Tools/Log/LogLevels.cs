﻿
namespace AugmentTest1.Tools.Log
{
    public enum LogLevels
    {
        Verbose         = -1,
        Information     = 0,
        Warning         = 1,
        Error           = 2,
        Critical        = 3
    }
}
