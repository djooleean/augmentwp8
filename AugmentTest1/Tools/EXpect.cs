﻿
namespace AugmentTest1.Tools
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Provides methods to check methods input parameters, and throw exceptions if needed.
    /// </summary>
    public static class Expect
    {
        /// <summary>
        /// Throws an 
        /// <see cref="T:System.ArgumentNullException" /> if the provided object
        /// is 
        /// <c>null</c>.
        /// </summary>
        /// <param name="obj">The reference to test.</param>
        /// <param name="paramName">The name of the parameter in the calling method.</param>
        /// <param name="callingMethod">The name of the calling method.</param>
        /// <exception cref="System.ArgumentNullException">Thrown if the provided object is <c>null</c>.</exception>
        public static void NotNull(object obj, string paramName, [CallerMemberName]string callingMethod = null)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(
                    paramName,
                    string.Format(CultureInfo.InvariantCulture, "Argument '{0}' cannot be null in method '{1}'.", paramName, callingMethod));
            }
        }

        /// <summary>
        /// Throws an 
        /// <see cref="T:System.ArgumentOutOfRangeException" /> if the provided string is <c>null</c> or empty (this includes
        /// white space.)
        /// </summary>
        /// <param name="target">The string reference to test.</param>
        /// <param name="paramName">The name of the parameter in the calling method.</param>
        /// <param name="callingMethod">The name of the calling method.</param>
        /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the provided object is <c>null</c>.</exception>
        public static void NotNullOrEmpty(string target, string paramName, [CallerMemberName]string callingMethod = null)
        {
            if (string.IsNullOrWhiteSpace(target))
            {
                throw new ArgumentOutOfRangeException(
                    paramName,
                    string.Format(CultureInfo.InvariantCulture, "Argument '{0}' cannot be null or empty in method '{1}'.", paramName, callingMethod));
            }
        }

        public static void ArrayNotEmpty(Array array, string paramName, [CallerMemberName]string callingMethod = null)
        {
            if (array == null || array.Length == 0)
                throw new ArgumentOutOfRangeException(
                    paramName,
                    string.Format(CultureInfo.InvariantCulture, "Argument '{0}' cannot be null or empty in method '{1}'.", paramName, callingMethod));
        }

        public static void ArrayNotEmpty(IEnumerable<object> array, string paramName, [CallerMemberName]string callingMethod = null)
        {
            if (array == null || array.Count() == 0)
                throw new ArgumentOutOfRangeException(
                    paramName,
                    string.Format(CultureInfo.InvariantCulture, "Argument '{0}' cannot be null or empty in method '{1}'.", paramName, callingMethod));
        }

    }
}
