﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tools.Web
{
    /// <summary>
    /// Temporary class that store "Tools.Web" constants & parameters
    /// TODO: Should be read from an embedded config file / database.
    /// </summary>
    public static class HttpConstants
    {        
        public const int DefaultRestClientTimeoutMs = 6000;
        public const string DefaultMediaType = "application/json";
        public const string DefaultUserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:19.0) Gecko/20100101 Firefox/19.0";

        public static class Headers
        {
            public static readonly string ResponseHeaders_Separator = "|";

            public static readonly string Accept = "Accept";
            public static readonly string ContentType = "Content-Type";
            public static readonly string Link = "Link";
            public static readonly string Authorization = "Authorization";
            public static readonly string UserAgent = "User-Agent";
            
        }

    }

}
