﻿

namespace AugmentTest1.Core.Navigation
{
    using System.Collections.Generic;

    public class NavigationContext
    {
        public NavigationContext()
        {
            Parameters = new Dictionary<NavigationParamaters, object>();
        }

        public Dictionary<NavigationParamaters, object> Parameters { get; }

        public void Reset()
        {
            Parameters.Clear();
        }

    }
}
