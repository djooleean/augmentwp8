﻿
namespace Tools.Web
{
    using System.Collections.Generic;

    /// <summary>
    /// Represent a generic Response for a REST web service call
    /// </summary>
    /// <typeparam name="T">Type of encapsulated request body response</typeparam>
    public class GenericRestResponse<T>
    {
        #region Constructor / init methods

        public GenericRestResponse()
        {
            InitializeInstance();
        }

        protected virtual void InitializeInstance()
        {
            ResponseHeaders = new Dictionary<string, string>();
            IsSuccess = false;
            ResponseMessage = string.Empty;
        }

        #endregion

        #region Public Properties

        public bool IsSuccess { get; set; }

        public string ResponseMessage { get; set; }

        public T ResponseData { get; set; }

        public Dictionary<string, string> ResponseHeaders { get; set; }

        #endregion

    }


}
