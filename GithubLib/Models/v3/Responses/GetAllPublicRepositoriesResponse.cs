﻿
namespace GithubLib.Models.v3.Responses
{
    using Repositories;
    using System.Collections.Generic;
    using Tools.Web;

    public class GetAllPublicRepositoriesResponse 
        : GenericRestResponse<List<PublicRepository>>, IPaginatedResponse
    {
        public GetAllPublicRepositoriesResponse()
        {

        }

        protected override void InitializeInstance()
        {
            base.InitializeInstance();

            CurrentPage = 0;
            MaxPages = 0;
            NextPageUri = string.Empty;
            PreviousPageUri = string.Empty;
            FirstPageUri = string.Empty;
        }

        public int CurrentPage { get; protected set; }
        public int MaxPages { get; protected set; }

        public string NextPageUri { get; protected set; }
        public string PreviousPageUri { get; protected set; }
        public string FirstPageUri { get; protected set; }

    }
}
