﻿using GithubLib.Models.v3.RateLimit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tools.Web;

namespace GithubLib.Models.v3.Responses
{
    public class GetCurrentRatesResponse : GenericRestResponse<CurrentRate>
    {

    }
}
