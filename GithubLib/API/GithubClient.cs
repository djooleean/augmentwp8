﻿
namespace GithubLib.API
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using GithubLib.API.Constants;
    using GithubLib.Models.v3.Repositories;
    using Tools.Web;
    using Exceptions;
    using Tools.Encoding;
    using Models.v3.RateLimit;
    using System.Text.RegularExpressions;
    using System.Diagnostics;
    using Models.v3.Follower;
    using System.Linq;

    public class GithubClient : IGithubClient, IDisposable
    {
        #region Private members

        IRestClient _restClient;

        private string GetAllPublicRepo_FirstLink_Endpoint;

        private string _getAllPublicRepo_NextLinkId;

        private static string _authenticatedHeader = "";

        #endregion

        #region Properties

        public bool IsReady
        {
            get;
            protected set;
        }

        #endregion

        #region Constructor + inits

        /// <summary>
        /// todo: use injection
        /// </summary>
        /// <param name="rclient"></param>
        public GithubClient()
        {
            InitializeRestClient();
        }

        private void InitializeRestClient()
        {
            _restClient = new RestClient(ApiConstants.v3.GithubBaseApiUri);
            
            // init get all public repo request
            GetAllPublicRepo_FirstLink_Endpoint = ApiConstants.v3.Endpoints.PublicRepositories;
            _getAllPublicRepo_NextLinkId = string.Empty;

            // other endpoints param's inits here
            //

            IsReady = true;
        }

        private void CheckIfReady()
        {
            if (!IsReady)
                throw new NotInitializedException("You need to call InitializeAsync() first");
        }

        #endregion


        #region Login / Logout methods

        public async Task<bool> LoginAsync(string username, string pwd)
        {
            CheckIfReady();

            // Get authorization header
            var authHeader = ComputeAuthorizationHeader(username, pwd);
            if (authHeader == string.Empty)
                return false;

            // store encoded header
            _authenticatedHeader = authHeader;

            try
            {
                // Try to get the current's user API requests rate
                //
                var response = await _restClient.HttpGetAsync<CurrentRate>(
                    ApiConstants.v3.Endpoints.RateLimit,
                    new Dictionary<string, string>(),
                    new Dictionary<string, string>()
                    {
                        { HttpConstants.Headers.Authorization, authHeader },
                        { HttpConstants.Headers.ContentType, ApiConstants.v3.Headers.Header_ContentType },
                        { HttpConstants.Headers.Accept, ApiConstants.v3.Headers.Header_AcceptGithub }
                    },
                    new System.Threading.CancellationToken()
                    );

                if (response.IsSuccess == false)
                {
                    return false;
                }
                else
                {
                    if (response.ResponseData.resources.core.limit == ApiConstants.v3.AnonymousRateLimit)
                        return false;

                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public void Logout()
        {
            _authenticatedHeader = string.Empty;
            _getAllPublicRepo_NextLinkId = string.Empty;
        }

        #endregion

        #region API methods

        public async Task<List<PublicRepository>> GetAllPublicRepositoriesAsync(PagingStep pagingOption = PagingStep.FirstPage)
        {
            CheckIfReady();

            // Request inits
            if (pagingOption == PagingStep.FirstPage)
                _getAllPublicRepo_NextLinkId = string.Empty;

            try
            {
                // Compute headers
                var hdrs = new Dictionary<string, string>()
                    {
                        { HttpConstants.Headers.Authorization, _authenticatedHeader },
                        { HttpConstants.Headers.ContentType, ApiConstants.v3.Headers.Header_ContentType },
                        { HttpConstants.Headers.Accept, ApiConstants.v3.Headers.Header_AcceptGithub }
                    };
                if (string.IsNullOrEmpty(_authenticatedHeader))
                    hdrs.Remove(HttpConstants.Headers.Authorization);

                // get repostitories
                var response = await _restClient.HttpGetAsync<List<PublicRepository>>(
                    ApiConstants.v3.Endpoints.PublicRepositories,
                    new Dictionary<string, string>()
                    {
                        { "since",  _getAllPublicRepo_NextLinkId}
                    },
                    hdrs,
                    new System.Threading.CancellationToken()
                    );

                // Success:
                if(response.IsSuccess)
                {
                    // read headers: save next link
                    if (response.ResponseHeaders != null && response.ResponseHeaders.Count > 0)
                    {
                        #region Read LINK header

                        if (response.ResponseHeaders.ContainsKey(ApiConstants.v3.Headers.Header_Link))
                        {
                            // read link header
                            var linkHdr = response.ResponseHeaders[ApiConstants.v3.Headers.Header_Link];

                            // parse header
                            if (!string.IsNullOrEmpty(linkHdr))
                            {
                                var comp = linkHdr.Split(',');
                                foreach(var c in comp)
                                {
                                    if (c.Contains(ApiConstants.v3.Headers.Attributes.Header_Link_NextAttribute))
                                    {
                                        var nextLinkValue = Regex.Match(c, @"\d+").Value;
                                        _getAllPublicRepo_NextLinkId = nextLinkValue;
                                        break;
                                    }
                                }
                            }
                        }

                        #endregion
                    }

                    // return repos
                    return response.ResponseData;
                }
                // Error happened:
                else
                {
                    // return empty list
                    return new List<PublicRepository>();
                }
            }
            finally
            {
                Debug.WriteLine("[Github API]: End of reading public repositories...");
            }
        }

        public async Task<List<FollowerInformation>> GetRepositoryFollowers(string repoOwnerName)
        {
            if (string.IsNullOrEmpty(repoOwnerName))
                return new List<FollowerInformation>();

            CheckIfReady();

            try
            {
                // Compute headers
                var hdrs = new Dictionary<string, string>()
                    {
                        { HttpConstants.Headers.Authorization, _authenticatedHeader },
                        { HttpConstants.Headers.ContentType, ApiConstants.v3.Headers.Header_ContentType },
                        { HttpConstants.Headers.Accept, ApiConstants.v3.Headers.Header_AcceptGithub }
                    };
                if (string.IsNullOrEmpty(_authenticatedHeader))
                    hdrs.Remove(HttpConstants.Headers.Authorization);

                // get followers
                var endpoint = ApiConstants.v3.Endpoints.Followers;
                endpoint = string.Format(endpoint, repoOwnerName);

                var response = await _restClient.HttpGetAsync<List<FollowerInformation>>(
                    endpoint,
                    new Dictionary<string, string>(),
                    hdrs,
                    new System.Threading.CancellationToken()
                    );

                // success
                if(response.IsSuccess)
                {
                    return
                        response.ResponseData;
                }
                else
                {
                    return new List<FollowerInformation>();
                }
            }
            finally
            {
                Debug.WriteLine("[Github API]: End of getting followers for: " + repoOwnerName);
            }
        }
        
        public async Task<RepositoryDetails> GetRepositoryDetails(PublicRepository repo)
        {
            CheckIfReady();

            if (repo == null)
                return null;

            try
            {
                // Compute headers
                var hdrs = new Dictionary<string, string>()
                    {
                        { HttpConstants.Headers.Authorization, _authenticatedHeader },
                        { HttpConstants.Headers.ContentType, ApiConstants.v3.Headers.Header_ContentType },
                        { HttpConstants.Headers.Accept, ApiConstants.v3.Headers.Header_AcceptGithub }
                    };
                if (string.IsNullOrEmpty(_authenticatedHeader))
                    hdrs.Remove(HttpConstants.Headers.Authorization);


                // get followers
                var endpoint = ApiConstants.v3.Endpoints.RepositoryDetails;
                endpoint = string.Format(endpoint, repo.owner.login, repo.name);

                var response = await _restClient.HttpGetAsync<RepositoryDetails>(
                    endpoint,
                    new Dictionary<string, string>(),
                    hdrs,
                    new System.Threading.CancellationToken()
                    );

                // success
                if (response.IsSuccess)
                {
                    return
                        response.ResponseData;
                }
                else
                {
                    return null;
                }
            }
            finally
            {
                Debug.WriteLine("[Github API]: End getting repository details...");
            }
        }

        public async Task<Dictionary<string, string>> GetRepositoryComments(RepositoryDetails repoDetails)
        {
            CheckIfReady();

            if (repoDetails == null)
                return null;

            try
            {
                // Compute headers
                var hdrs = new Dictionary<string, string>()
                    {
                        { HttpConstants.Headers.Authorization, _authenticatedHeader },
                        { HttpConstants.Headers.ContentType, ApiConstants.v3.Headers.Header_ContentType },
                        { HttpConstants.Headers.Accept, ApiConstants.v3.Headers.Header_AcceptGithub }
                    };
                if (string.IsNullOrEmpty(_authenticatedHeader))
                    hdrs.Remove(HttpConstants.Headers.Authorization);


                // get followers
                var endpoint = ApiConstants.v3.Endpoints.RepositoryDetailsComments;
                endpoint = string.Format(endpoint, repoDetails.owner.login, repoDetails.name);

                var response = await _restClient.HttpGetAsync<List<RepoComment>>(
                    endpoint,
                    new Dictionary<string, string>(),
                    hdrs,
                    new System.Threading.CancellationToken()
                    );

                // success
                if (response.IsSuccess)
                {
                    var lstComments = response.ResponseData.Select(
                        currentRepo => new KeyValuePair<string, string>(currentRepo.user.login, currentRepo.body))
                        .ToList();

                    Dictionary<string, string> resultDic = new Dictionary<string, string>();
                    int index = 1;
                    foreach (var e in lstComments)
                    {
                        resultDic.Add(index.ToString() + "- " + e.Key, e.Value);
                        index++;
                    }
                    return resultDic;
                }
                else
                {
                    return null;
                }
            }
            finally
            {
                Debug.WriteLine("[Github API]: End getting repository details...");
            }
        }

        #endregion

        #region Private methods

        private string ComputeAuthorizationHeader(string login, string pwd)
        {
            var cred = login + ":" + pwd;
            var enc = Base64Encoder.Encode(cred);

            if (string.IsNullOrEmpty(enc))
                return "";
            else
                enc = "Basic " + enc;

            return enc;
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            IsReady = false;

            if ( _restClient != null)
            {
                _restClient.Dispose();
                _restClient = null;
            }            
        }


        #endregion
    }
}
