﻿
namespace AugmentTest1.Core.Setup
{
    using AppContext;
    using GithubLib.API;
    using NativeServices;
    using Navigation;
    using Tools.Log;

    /// <summary>
    /// IOC Module that initializes app's services 
    /// </summary>
    public class ServicesModule : Ninject.Modules.NinjectModule
    {
        public override void Load()
        {
            // our default logger
            Kernel.Bind<ILogger>().To<AugmentLogger>().InSingletonScope();

            // navigation service (singleton)
            Kernel.Bind<INavigationService>().To<DefaultNavigationService>().InSingletonScope();

            // app state (singleton)
            Kernel.Bind<IAppState>().To<AugmentAppState>().InSingletonScope();

            // Github client
            Kernel.Bind<IGithubClient>().To<GithubClient>();
        }

    }


}
