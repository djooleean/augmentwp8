﻿
namespace AugmentTest1.ViewModels
{
    using AugmentTest1.Tools;

    public interface IMainPageViewModel
    {
        string Login { get; set; }

        string Password { get; set; }


        DelegateCommand LoginCommand { get; }

        DelegateCommand AnonymousLoginCommand { get; }

    }
}
