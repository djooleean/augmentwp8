﻿
namespace AugmentTest1.ViewModels
{
    using Core.AppContext;
    using Core.Navigation;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using System.Threading.Tasks;
    using Tools;

    public abstract class BaseViewModel : IBaseViewModel, INotifyPropertyChanged
    {
        #region Private members

        private bool _isLoadingData;

        private string _loadingMessage;

        private INavigationService _navigationService;

        #endregion

        #region Constructor

        public BaseViewModel(
            INavigationService navigationService,
            IAppState appState
            )
        {
            Expect.NotNull(navigationService, "Navigation service");
            Expect.NotNull(appState, "Application State");

            NavService = navigationService;

            CurrentState = appState;
        }


        #endregion

        #region PropertyChanged 

        private PropertyChangedEventHandler _propertyChangedEventHandler;

        public event PropertyChangedEventHandler PropertyChanged
        {
            add
            {
                _propertyChangedEventHandler -= value;
                _propertyChangedEventHandler += value;
            }
            remove
            {
                _propertyChangedEventHandler -= value;
            }
        }

        /// <summary>
        /// Called when a property value has changed, to notify all listeners
        /// </summary>
        /// <param name="propertyName">Name of the property that changed</param>
        protected void RaisePropertyChanged(string propertyName)
        {
            if (_propertyChangedEventHandler != null)
                _propertyChangedEventHandler(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Called to changed the value of a specific VM's property
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="property"></param>
        /// <param name="value"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public bool Set<T>(ref T property, T value, [CallerMemberName] string propertyName = "")
        {
            if (object.Equals(property, value))
            {
                return false;
            }

            property = value;

            this.RaisePropertyChanged(propertyName);

            return true;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Indicates if the current ViewModel is loading data
        /// </summary>
        public bool IsLoading
        {
            get { return _isLoadingData; }
            protected set
            {
                Set(ref _isLoadingData, value);
            }
        }

        /// <summary>
        /// Gets / sets the VM's loading message
        /// </summary>
        public string LoadingMessage
        {
            get { return _loadingMessage; }
            set
            {
                Set(ref _loadingMessage, value);
            }
        }


        public INavigationService NavService
        {
            get { return _navigationService; }
            private set { Set(ref _navigationService, value); }
        }


        public IAppState CurrentState
        {
            get;
            private set;
        }


        #endregion




        public async Task<bool> ManageBackKeyPressedAsync()
        {
            // perform current page 'back
            await OnBackKeyPressedAsync();

            // Navigate back
            if (_navigationService.CanGoBack == true)
            {
                _navigationService.GoBack();
                return true;
            }
            else
                return false;
        }

        protected abstract Task OnBackKeyPressedAsync();

        public abstract Task<bool> LoadAsync();

        public abstract Task<bool> RefreshAsync();

        public abstract void Clean();

    }
}
