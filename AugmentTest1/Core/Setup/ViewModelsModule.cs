﻿
namespace AugmentTest1.Core.Setup
{
    using AugmentTest1.ViewModels;

    /// <summary>
    /// IOC module that initializes app's ViewModels
    /// </summary>
    public class ViewModelsModule : Ninject.Modules.NinjectModule
    {
        public override void Load()
        {
            // binding: used by pages navigation system
            //
            Kernel.Bind<BaseViewModel>().To<MainPageViewModel>().InSingletonScope().Named("MainPage");
            Kernel.Bind<BaseViewModel>().To<RepoListViewModel>().InSingletonScope().Named("RepoListPage");
            Kernel.Bind<BaseViewModel>().To<RepositoryDetailViewModel>().InSingletonScope().Named("RepositoryDetailPage");            
        }

    }
}
