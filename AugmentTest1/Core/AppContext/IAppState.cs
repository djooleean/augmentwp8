﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AugmentTest1.Core.AppContext
{
    public interface IAppState
    {

        IAppStateData StateData { get; }

        

        void LoadCurrentState();

        void SaveCurrentState();

    }

}
