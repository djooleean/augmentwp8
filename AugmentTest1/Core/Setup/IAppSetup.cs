﻿

namespace AugmentTest1.Core.Setup
{
    using Ninject;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface describing behaviors to initialize this application
    /// </summary>
    public interface IAppSetup
    {
        /// <summary>
        /// Used to create the app's IOC container.
        /// (Should be then stored as unique instance of the app)
        /// </summary>
        /// <returns>NInject IOC container</returns>
        StandardKernel CreateIocContainer();

        void RegisterAppLogger();


        bool IsNetworkAvailable { get; }

    }
}
