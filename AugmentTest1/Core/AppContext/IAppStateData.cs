﻿
namespace AugmentTest1.Core.AppContext
{
    public interface IAppStateData
    {
        void Reset();

        bool IsLogged { get; set; }

        string UserLogin { get; set; }

    }
}
