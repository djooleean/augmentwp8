﻿
namespace AugmentTest1.Tools.Log
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Text;
    using System.Threading.Tasks;

    public interface ILogListener
    {
        LogLevels Filter { get; set; }

        /// <summary>
        /// Log a message of a specific level asynchronously
        /// </summary>
        /// <param name="level"></param>
        /// <param name="message"></param>
        /// <param name="tags"></param>
        /// <param name="memberName"></param>
        /// <param name="sourceFilePath"></param>
        /// <param name="sourceLineNumber"></param>
        /// <see cref="T:System.Runtime.CompilerServices.CallerMemberName" />
        /// <see cref="T:System.Runtime.CompilerServices.CallerFilePath" /> 
        /// <see cref="T:System.Runtime.CompilerServices.CallerLineNumber" /> 
        /// <returns></returns>
        Task LogAsync(
            LogLevels level,
            string message,
            Dictionary<string,string> tags,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string sourceFilePath = "",
            [CallerLineNumber] int sourceLineNumber = 0);
        
        /// <summary>
        /// Log an exception asynchronously
        /// </summary>
        /// <param name="exception"></param>
        /// <param name="memberName"></param>
        /// <param name="sourceFilePath"></param>
        /// <param name="sourceLineNumber"></param>
        /// <returns></returns>
        Task ErrorAsync(
            Exception exception,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string sourceFilePath = "",
            [CallerLineNumber] int sourceLineNumber = 0);

    }
}
