﻿
namespace AugmentTest1.Core.Setup
{
    using Ninject;
    using System.Collections.Generic;
    using Tools.Log;
    using Tools.Log.Listeners;
    using System.Threading.Tasks;
    using System.Net.NetworkInformation;
    using AppContext;

    public class AppSetup : IAppSetup
    {
        private StandardKernel _iocKernel;

        /// <summary>
        /// Indicates if there is an available connection
        /// </summary>
        public bool IsNetworkAvailable
        {
            get
            {
                return NetworkInterface.GetIsNetworkAvailable();
            }
        }



        /// <summary>
        /// Initialize IOC container; 
        /// Register current application dependencies  and return the container.        
        /// </summary>        
        /// <remarks>TO CALL before any other setup method.</remarks>
        /// <returns>Ninject IKernel (the container)</returns>
        public StandardKernel CreateIocContainer()
        {
            if (_iocKernel == null)
            {
                // create container
                _iocKernel = new StandardKernel();

                // Load ioc modules
                RegisterDependencies(_iocKernel);
            }

            return _iocKernel;
        }        

        /// <summary>
        /// Initialize default app loggers
        /// </summary>
        /// <exception cref="Exception">If 'CreateIocContainer()' has not been called before.</exception>
        public void RegisterAppLogger()
        {
            if (_iocKernel != null)
            {
                var logger = _iocKernel.Get<ILogger>();
                logger.RegisterListener(new ConsoleListener { Filter = LogLevels.Verbose });
            }
            else
            {
                throw new System.Exception("You must call 'CreateIocContainer' before.");
            }
        }

        public void InitializeAppState()
        {
            if (_iocKernel != null)
            {
                var appState = _iocKernel.Get<IAppState>();
                appState.LoadCurrentState();
            }
            else
            {
                throw new System.Exception("You must call 'CreateIocContainer' before.");
            }
        }

        #region Private methods

        /// <summary>
        /// Loads the modules containing service types registration.
        /// </summary>
        /// <param name="kernel"></param>
        protected void RegisterDependencies(StandardKernel kernel)
        {
            // App setup: declare all IOC modules to register
            List<Ninject.Modules.NinjectModule> setupModules = new List<Ninject.Modules.NinjectModule>()
            {
                new ServicesModule(),
                new ViewModelsModule()
            };

            // Load all app setup modules
            kernel.Load(setupModules);
        }

        #endregion

    }

}
