﻿
using GithubLib.Models.v3.Follower;
using GithubLib.Models.v3.Repositories;
using System.Collections.ObjectModel;

namespace AugmentTest1.ViewModels
{

    public interface IRepositoryDetailViewModel
    {
        PublicRepository SelectedRepository { get; }

        ObservableCollection<FollowerInformation> Followers { get; }

        ObservableCollection<RepoCommentViewModel> Comments { get; }

        bool HasIssues { get; }
        string NbIssues { get; }
        bool HasDownloads { get; }
        bool HasWiki { get; }



    }



}
