﻿
namespace AugmentTest1.Core.Navigation
{
    using System;
    using System.Threading.Tasks;
    public interface INavigationService
    {
        NavigationContext Context { get; }

        bool CanGoBack { get; }

        void Navigate(Type sourcePageType);

        void Navigate(Type sourcePageType, NavigationContext parameter);

        void GoBack(NavigationContext parameters = null);


        Task ShowPopup(string message);
    }
}
