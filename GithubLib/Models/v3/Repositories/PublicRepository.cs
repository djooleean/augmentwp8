﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GithubLib.Models.v3.Repositories
{
    /// <summary>
    /// (Auto generated)
    /// </summary>
    public class PublicRepository
    {
        public int id { get; set; }
        public RepositoryOwner owner { get; set; }
        public string name { get; set; }
        public string full_name { get; set; }
        public string description { get; set; }
        public bool @private { get; set; }
        public bool fork { get; set; }
        public string url { get; set; }
        public string html_url { get; set; }
    }

    


}
