﻿
namespace AugmentTest1.NativeServices
{
    using System;
    using AugmentTest1.Core.AppContext;
    using Windows.Storage;
    using Newtonsoft.Json;

    public class AugmentAppState : IAppState
    {

        private ApplicationDataContainer _stateContainer;
        private readonly string stateContainerKey = "appLocalState";

        #region Const app state property keys

        private static readonly string appStateKey = "ask";

        #endregion
        
        public AugmentAppState()
        {
            // initialize app's state container
            _stateContainer = 
                Windows.Storage.ApplicationData.Current.LocalSettings.CreateContainer(stateContainerKey, Windows.Storage.ApplicationDataCreateDisposition.Always);

            LoadCurrentState();
        }
        
        private void DefaultInitialization()
        {
            StateData = new AugmentAppStateData();
        }


        public IAppStateData StateData
        {
            get;
            private set;
        }


        public void LoadCurrentState()
        {
            object data = new object();

            if(_stateContainer.Values.TryGetValue(appStateKey, out data)==false)
            {
                DefaultInitialization();
            }
            else
            {
                StateData = JsonConvert.DeserializeObject<AugmentAppStateData>(data.ToString());
            }
        }

        public void SaveCurrentState()
        {
            try
            {
                var toSer = JsonConvert.SerializeObject(StateData);
                _stateContainer.Values[appStateKey] = toSer;
            }
            finally
            {

            }
        }


    }

}
