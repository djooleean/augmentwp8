﻿
namespace AugmentTest1.Core.Navigation
{
    using System;
    using System.Threading.Tasks;
    using Windows.UI.Popups;
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;

    public class DefaultNavigationService : INavigationService
    {
        private NavigationContext _navigationContext;

        /// <summary>
        /// Gets the current's page navigation context
        /// </summary>
        public NavigationContext Context
        {
            get
            {
                return _navigationContext;
            }
            private set
            {
                _navigationContext = value;
            }
        }

        public bool CanGoBack
        {
            get
            {
                return ((Frame)Window.Current.Content).CanGoBack;
            }
        }


        public void Navigate(Type sourcePageType)
        {
            if(Context != null) Context.Reset();
            ((Frame)Window.Current.Content).Navigate(sourcePageType);
        }

        public void Navigate(Type sourcePageType, NavigationContext parameters)
        {
            if (Context != null) Context.Reset();
            Context = parameters;
            ((Frame)Window.Current.Content).Navigate(sourcePageType);
        }

        public void GoBack(NavigationContext parameters = null)
        {
            if (Context != null) Context.Reset();
            Context = parameters;
            ((Frame)Window.Current.Content).GoBack();
        }


        public async Task ShowPopup(string message)
        {
            MessageDialog dlg = new MessageDialog(message);
            await dlg.ShowAsync();
        }

    }
}
