﻿
namespace GithubLib.API
{
    using Constants;
    using Models.v3.Follower;
    using Models.v3.Repositories;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IGithubClient
    {
        #region Properties

        /// <summary>
        /// Inidicates if the Github client is initialized
        /// </summary>
        bool IsReady { get; }

        #endregion

        /// <summary>
        /// Get all public repositories
        /// </summary>
        /// <param name="pagingOption">The pagination option</param>
        /// <returns>List of public repositories (can be empty)</returns>
        Task<List<PublicRepository>> GetAllPublicRepositoriesAsync(PagingStep pagingOption);
        
        Task<List<FollowerInformation>> GetRepositoryFollowers(string repoOwnerName);

        Task<RepositoryDetails> GetRepositoryDetails(PublicRepository repo);

        Task<Dictionary<string, string>> GetRepositoryComments(RepositoryDetails repoDetails);

        Task<bool> LoginAsync(string username, string pwd);

        void Logout();

    }
}
