﻿
namespace Tools.Encoding
{
    public static class Base64Encoder
    {

        public static string Encode(string toEncode)
        {
            if (string.IsNullOrEmpty(toEncode))
                return string.Empty;
            
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(toEncode);

            return System.Convert.ToBase64String(plainTextBytes);
        }

    }
}
