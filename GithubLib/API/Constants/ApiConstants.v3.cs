﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GithubLib.API.Constants
{
    /// <summary>
    /// TODO: Refactor (move constants to config file)
    /// </summary>
    public static partial class ApiConstants
    {
        public static class v3
        {
            public static readonly string GithubBaseApiUri = @"https://api.github.com/";
            public static readonly int AnonymousRateLimit = 60;
            public static readonly int BasicAuthRateLimit = 5000;

            public static class Headers
            {
                public static readonly string Header_ContentType = "application/json";
                public static readonly string Header_AcceptGithub = "application/vnd.github.v3+json";
                public static readonly string Header_Link = "Link";
                public static readonly string Header_XRateLimit = "X-RateLimit-Limit";
                public static readonly string Header_XRateRemaining = "X-RateLimit-Remaining";

                public static class Attributes
                {
                    public static readonly string Header_Link_NextAttribute = "rel=\"next\"";
                    public static readonly string Header_Link_FirstAttribute = "rel=\"first\"";
                }
            }

            public static class Endpoints
            {                
                public static readonly string Followers = "/users/{0}/followers";
                public static readonly string RepositoryDetails = "/repos/{0}/{1}";
                public static readonly string RepositoryDetailsComments = "/repos/{0}/{1}/comments";
                public static readonly string PublicRepositories = "/repositories";
                public static readonly string RateLimit = "/rate_limit";

            }

        }
    }
}
