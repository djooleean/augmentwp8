﻿
namespace Tools.Web
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface that defines a generic REST client
    /// </summary>
    public interface IRestClient : IDisposable
    {
        /// <summary>
        /// Send a GET request to the desired REST web service (defined by the 'endpoint' parameter).
        /// </summary>
        /// <typeparam name="T">Generic type of the expected response</typeparam>
        /// <param name="wsEndpoint"></param>
        /// <param name="parameters"></param>
        /// <param name="requestHeaders"></param>
        /// <param name="cancelToken"></param>        
        /// <returns></returns>
        Task<GenericRestResponse<T>> HttpGetAsync<T>(
            string wsEndpoint, 
            Dictionary<string, string> parameters,
            Dictionary<string, string> requestHeaders,
            CancellationToken cancelToken
            );

        /// <summary>
        /// Send a POST request to the desired REST web service (defined by the 'endpoint' parameter).
        /// </summary>
        /// <typeparam name="T">Generic type of the response returned by the POST command</typeparam>
        /// <param name="wsEndpoint">URL to target</param>
        /// <param name="parameters">URL parameters used with the POST operation</param>
        /// <param name="requestHeaders"></param>
        /// <param name="data">The body data</param>
        /// <param name="cancelToken">Cancellation token to interrupt the asynchronous POST</param>
        /// <returns>Generic Response of type T</returns>
        Task<GenericRestResponse<T>> HttpPostAsync<T>(
            string wsEndpoint, 
            Dictionary<string, string> parameters,
            Dictionary<string, string> requestHeaders,
            object data, 
            CancellationToken cancelToken
            );

    }

}
