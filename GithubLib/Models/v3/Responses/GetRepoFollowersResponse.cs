﻿
using GithubLib.Models.v3.Follower;
using Tools.Web;

namespace GithubLib.Models.v3.Responses
{
    public class GetRepoFollowersResponse : GenericRestResponse<FollowerInformation>
    {
    }
}
