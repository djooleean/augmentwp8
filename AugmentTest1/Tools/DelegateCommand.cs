﻿
namespace AugmentTest1.Tools
{
    using System;
    using System.Windows.Input;

    public class DelegateCommand : ICommand
    {
        #region Private members

        private readonly Action<object> _executeMethod = null;

        private readonly Predicate<object> _canExecuteMethod = null;

        #endregion

        public DelegateCommand(
            Action<Object> executeMethod, 
            Predicate<object> canExecuteMethod 
            )
        {
            Expect.NotNull(executeMethod, "execute Method");

            _executeMethod = executeMethod;

            _canExecuteMethod = canExecuteMethod;
        }

        #region CanExecute

        public bool CanExecute(object parameter)
        {
            return _canExecuteMethod == null ? true : _canExecuteMethod(parameter);
        }

        public event EventHandler CanExecuteChanged;

        #endregion

        public void Execute(object parameter)
        {
            if (_executeMethod != null)
                _executeMethod(parameter);
        }

        /// <summary>
        /// Call to 'compute' CanExecute
        /// </summary>
        public void RaiseCanExecuteChanged()
        {
            if (CanExecuteChanged != null)
                CanExecuteChanged(this, EventArgs.Empty);
        }

    }

}