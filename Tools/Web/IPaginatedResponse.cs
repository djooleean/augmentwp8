﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tools.Web
{
    public interface IPaginatedResponse
    {

        int CurrentPage { get; }
        int MaxPages { get; }

        string NextPageUri { get; }
        string PreviousPageUri { get; }
        string FirstPageUri { get; }
    }

}
