﻿using AugmentTest1.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using GithubLib.Models.v3.Repositories;

namespace AugmentTest1.ViewModels
{
    public class RepoList : IncrementalLoadingDataSource
    {
        Func<Task<List<PublicRepository>>> _loadMoreFuncAsync;
        bool _canLoadMore;

        public RepoList(Func<Task<List<PublicRepository>>> loadMoreFuncAsync)
        {
            Expect.NotNull(loadMoreFuncAsync, "Load more Function");

            _loadMoreFuncAsync = loadMoreFuncAsync;

            _canLoadMore = true;
        }

        protected override bool HasMoreItemsOverride()
        {
            return _canLoadMore;   
        }

        protected override async Task<IList<object>> LoadMoreItemsOverrideAsync(CancellationToken c, uint count = 100)
        {
            List<object> items = new List<object>();

            var res = await _loadMoreFuncAsync();
            if(res != null && res.Count >0)
            {
                _canLoadMore = true;
                foreach (var item in res)
                    items.Add(item);
            }
            else
            {
                _canLoadMore = false;
            }
            
            return items;
        }
    }
}
