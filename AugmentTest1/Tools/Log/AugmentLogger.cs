﻿
namespace AugmentTest1.Tools.Log
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// The default application's logger
    /// </summary>
    public class AugmentLogger : ILogger
    {
        #region Private members

        private List<ILogListener> _listeners;

        private SemaphoreSlim _semLocker = new SemaphoreSlim(1);

        #endregion

        #region Constructor

        /// <summary>
        /// Initialize the logger. By default it is Enabled.
        /// Default log level is: Information
        /// </summary>
        public AugmentLogger()
        {
            IsEnabled = true;

            Filter = LogLevels.Information;

            _listeners = new List<ILogListener>();
        }


        #endregion

        #region Public properties

        /// <summary>
        /// Indicates if the logger is active or not
        /// </summary>
        public bool IsEnabled
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets / sets the logger filter
        /// </summary>
        public LogLevels Filter
        {
            get; set;

        }

        #endregion


        public async Task LogAsync(LogLevels level, string message, Dictionary<string, string> tags, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
        {
            await _semLocker.WaitAsync();

            try
            {
                foreach (var currentListener in _listeners)
                {
                    await currentListener.LogAsync(level, message, tags, memberName, sourceFilePath, sourceLineNumber);
                }
            }
            finally
            {
                _semLocker.Release();
            }
        }

        public async Task ErrorAsync(Exception exception, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
        {
            await _semLocker.WaitAsync();

            try
            {
                foreach(var currentListener in _listeners)
                {
                    await currentListener.ErrorAsync(exception, memberName, sourceFilePath, sourceLineNumber);
                }
            }
            finally
            {
                _semLocker.Release();
            }
        }


        /// <summary>
        /// Add a listener that will handle incoming log events
        /// </summary>
        /// <param name="listener">The listener to manage</param>
        public void RegisterListener(ILogListener listener)
        {
            _semLocker.Wait();

            try{
                if (listener == null)
                    return;

                if (_listeners.Contains(listener))
                    return;

                _listeners.Add(listener);
            }
            finally
            {
                _semLocker.Release();
            }
        }

        /// <summary>
        /// Remove a specific listener
        /// </summary>
        /// <param name="listener">Listener to remove</param>
        public void RemoveListener(ILogListener listener)
        {
            _semLocker.Wait();

            try{
                if (listener == null)
                    return;

                if (!_listeners.Contains(listener))
                    return;

                _listeners.Remove(listener);
            }
            finally
            {
                _semLocker.Release();
            }
               
        }

        /// <summary>
        /// Remove all specific listeners of type <paramref name="T"/>
        /// </summary>
        /// <typeparam name="T">Type of listeners to remove</typeparam>
        public void RemoveListener<T>() where T : ILogListener
        {
            _semLocker.Wait();

            try
            {
                _listeners.RemoveAll(
                    listenerIn => { return (listenerIn.GetType() == typeof(T)); });
            }
            finally
            {
                _semLocker.Release();
            }
        }

        /// <summary>
        /// Clean and reset the logger
        /// </summary>
        public void Reset()
        {
            _semLocker.Wait();

            try
            {
                _listeners.Clear();

                Filter = LogLevels.Information;
            }
            finally
            {
                _semLocker.Release();
            }
        }


    }
}
