﻿
namespace AugmentTest1.Tools.Log
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public interface ILogger : ILogListener
    {
        bool IsEnabled { get; }


        void RegisterListener(ILogListener listener);

        void RemoveListener(ILogListener listener);

        void RemoveListener<T>() where T : ILogListener;

        void Reset();

    }

}
