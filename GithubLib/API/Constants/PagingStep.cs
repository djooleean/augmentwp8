﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GithubLib.API.Constants
{
    public enum PagingStep
    {
        FirstPage,
        NextPage,
        LastPage
    }
}
