﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AugmentTest1.Core.AppContext;
using AugmentTest1.Core.Navigation;
using GithubLib.Models.v3.Repositories;
using GithubLib.API;
using AugmentTest1.Tools;
using System.Collections.ObjectModel;
using GithubLib.Models.v3.Follower;

namespace AugmentTest1.ViewModels
{
    public class RepositoryDetailViewModel : BaseViewModel, IRepositoryDetailViewModel
    {
        #region Private members

        PublicRepository _repo;

        IGithubClient _gitClient;

        private ObservableCollection<FollowerInformation> _followers;

        private ObservableCollection<RepoCommentViewModel> _comments;

        private bool _hasIssues;
        private string _nbIssues;
        private bool _hasDownloads;
        private bool _hasWiki;

        #endregion

        public RepositoryDetailViewModel(
            INavigationService navigationService, 
            IAppState appState,
            IGithubClient gitClient
            ) 
            : base(navigationService, appState)
        {
            Expect.NotNull(gitClient, "gitClient");

            _repo = null;

            _gitClient = gitClient;

            Followers = new ObservableCollection<FollowerInformation>();

            Comments = new ObservableCollection<RepoCommentViewModel>();
        }


        #region Properties

        public PublicRepository SelectedRepository
        {
            get { return _repo; }
            private set
            {
                Set(ref _repo, value);
            }
        }

        public ObservableCollection<FollowerInformation>  Followers
        {
            get
            {
                return _followers;
            }
            private set
            {
                Set(ref _followers, value);
            }
        }

        public ObservableCollection<RepoCommentViewModel> Comments
        {
            get { return _comments; }
            private set
            {
                Set(ref _comments, value);
            }
        }


        public bool HasIssues
        {
            get { return _hasIssues; }
            set { Set(ref _hasIssues, value); }
        }
        public string NbIssues
        {
            get { return _nbIssues; }
            set { Set(ref _nbIssues, value); }
        }
        public bool HasDownloads
        {
            get { return _hasDownloads; }
            set { Set(ref _hasDownloads, value); }
        }
        public bool HasWiki
        {
            get { return _hasWiki; }
            set { Set(ref _hasWiki, value); }
        }
        #endregion


        public override void Clean()
        {
            // clean details
        }

        public override async Task<bool> LoadAsync()
        {
            try
            {
                IsLoading = true;

                // Load from navigation context
                if (NavService.Context.Parameters.ContainsKey(NavigationParamaters.SelectedPublicRepository))
                {
                    // get Repository summary
                    SelectedRepository =
                        NavService.Context.Parameters[NavigationParamaters.SelectedPublicRepository] as PublicRepository;
                    
                    if (SelectedRepository != null && SelectedRepository.owner != null)
                    {
                        // load followers
                        Followers.Clear();

                        var res = await _gitClient.GetRepositoryFollowers(SelectedRepository.owner.login);
                        foreach (var f in res)
                        {
                            Followers.Add(f);

                            // load max 10 followers
                            if (Followers.Count > 10)
                                break;                            
                        }

                        // load repo details
                        var repoDetails = await _gitClient.GetRepositoryDetails(SelectedRepository);
                        if(repoDetails != null)
                        {
                            // set properties
                            HasDownloads    = repoDetails.has_downloads;
                            NbIssues        = repoDetails.open_issues_count.ToString();
                            HasIssues       = repoDetails.has_issues;
                            HasWiki         = repoDetails.has_wiki;

                            // load comments
                            var comments =
                                await _gitClient.GetRepositoryComments(repoDetails);

                            if(comments != null)
                            {
                                Comments.Clear();
                                foreach (var c in comments)
                                    Comments.Add(new RepoCommentViewModel { Author = c.Key, Message = c.Value });
                            }
                        }
                    }

                }
                else
                {
                    // need to re select a repository
                    NavService.GoBack();
                    return false;
                }

                return true;
            }
            finally
            {
                IsLoading = false;
            }
        }

        public override Task<bool> RefreshAsync()
        {
            // same as load
            return LoadAsync();
        }

        protected override Task OnBackKeyPressedAsync()
        {
            return Task.FromResult(true);
        }
    }
}
