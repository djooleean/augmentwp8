﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AugmentTest1.Core.AppContext;
using AugmentTest1.Core.Navigation;
using System.Collections.ObjectModel;
using GithubLib.Models.v3.Repositories;
using GithubLib.API;
using AugmentTest1.Tools;
using AugmentTest1.Views;

namespace AugmentTest1.ViewModels
{
    public class RepoListViewModel : BaseViewModel, IRepoListViewModel
    {
        #region Private members

        private RepoList _repositories;
        
        private PublicRepository _selectedRepo;

        private IGithubClient _gitClient;

        private DelegateCommand _viewRepoCommand;

        #endregion

        public RepoListViewModel(
            INavigationService navigationService, 
            IAppState appState, 
            IGithubClient gitClient
            ) 
            : base(navigationService, appState)
        {
            Expect.NotNull(gitClient, "gitClient");

            //Repositories = new ObservableCollection<PublicRepository>();
            Repositories = new RepoList(
                async () =>
                {
                    return await _gitClient.GetAllPublicRepositoriesAsync(GithubLib.API.Constants.PagingStep.NextPage);
                });

            _gitClient = gitClient;

            InitializeCommands();
        }



        private void InitializeCommands()
        {
            ViewRepoDetailsCommand = new DelegateCommand(
                (param) => ExecuteViewRepoDetailsCommand(),
                (param) => true
                );
        }

        #region Properties

        public PublicRepository SelectedRepository
        {
            get { return _selectedRepo; }
            set
            {
                Set(ref _selectedRepo, value);
                RaisePropertyChanged("SelectedItemEnabled");
            }
        }

        public bool SelectedItemEnabled
        {
            get
            {
                return (SelectedRepository != null);
            }
        }

        /// <summary>
        /// Gets github public repository list
        /// </summary>
        //public ObservableCollection<PublicRepository> Repositories
        //{
        //    get { return _repositories; }
        //    private set
        //    {
        //        Set(ref _repositories, value);
        //    }
        //}
        public RepoList Repositories
        {
            get { return _repositories; }
            private set
            {
                Set(ref _repositories, value);
            }
        }

        public DelegateCommand ViewRepoDetailsCommand
        {
            get { return _viewRepoCommand; }
            private set
            {
                Set(ref _viewRepoCommand, value);
            }
        }

        #endregion

        #region Define commands


        private void ExecuteViewRepoDetailsCommand ()
        {
            if (SelectedRepository == null)
                return;

            // set navigation context
            NavigationContext navCtx = new NavigationContext();
            navCtx.Parameters.Add(NavigationParamaters.SelectedPublicRepository, SelectedRepository);

            // Navigate to next page
            NavService.Navigate(typeof(RepositoryDetailPage), navCtx);
        }


        #endregion


        #region BaseViewModel Implementation

        public override void Clean()
        {
            //Repositories.Clear();
        }

        public override async Task<bool> LoadAsync()
        {
            if (IsLoading)
                return false;

            try
            {
                IsLoading = true;

                //await Task.Delay(1000);

                //// load repos
                //var result = await _gitClient.GetAllPublicRepositoriesAsync(GithubLib.API.Constants.PagingStep.NextPage);

                //// Update UI repository list
                ////
                ////this.Repositories.Clear();
                //foreach (var it in result)
                //    this.Repositories.Add(it);

                await Task.Delay(1000);

                return true;
            }
            finally
            {
                IsLoading = false;
            }
        }

        public override Task<bool> RefreshAsync()
        {
            return Task.FromResult(true);
        }

        protected override Task OnBackKeyPressedAsync()
        {
            // log out from the git client (need to re authenticate)
            _gitClient.Logout();

            return Task.FromResult(true);
        }

        #endregion

    }
}
